<?php

namespace Contato\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="tb_contatos")
 */
class Contato
{
    /**
     * @var int
     * @access protected
     *
     * @ORM\Id
     * @ORM\Column(name="id_contato", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @var string
     * @access protected
     *
     * @ORM\Column(type="string", length=50)
     */
    protected $nome;
    
    /**
     * @var Telefone[]
     * @access protected
     *
     * @ORM\ManyToMany(targetEntity="Telefone")
     * @ORM\JoinTable(
     *  name="tb_contato_telefone",
     *  joinColumns={@ORM\JoinColumn(name="id_contato", referencedColumnName="id_contato")},
     *  inverseJoinColumns={@ORM\JoinColumn(name="id_telefone", referencedColumnName="id_telefone")}
     * )
     */
    protected $telefones;
}
