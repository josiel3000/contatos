<?php

namespace Contato\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="tb_telefones")
 */
class Telefone
{
    /**
     * @var int
     * @access protected
     *
     * @ORM\Id
     * @ORM\Column(name="id_contato", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @var string
     * @access protected
     *
     * @ORM\Column(type="string", length=2)
     */
    protected $ddd;
    
    /**
     * @var string
     * @access protected
     *
     * @ORM\Column(type="string", length=9)
     */
    protected $numero;
}
