<?php

namespace Contato\Form;

use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterInterface;
use Zend\InputFilter\InputFilterProviderInterface;

abstract class BaseFieldset extends Fieldset implements InputFilterProviderInterface
{
    public function __construct($name = null)
    {
        parent::__construct($name);
    }
}
