<?php

namespace Contato\Form;

use Zend\Form\Form;
use Zend\ServiceManager\ServiceManagerInterface;
use Zend\ServiceManager\ServiceManagerAwareInterface;

abstract class BaseForm extends Form
{
    protected $sm;
    
    public function getServiceManager()
    {
        return $this->sm;
    }
    
    public function setServiceManager(ServiceManagerInterface $sm)
    {
        $this->sm = $sm;
    }
}
