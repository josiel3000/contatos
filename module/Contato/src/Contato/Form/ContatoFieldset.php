<?php

namespace Contato\Form;

use Zend\Form\Element\Hidden;
use Zend\Form\Element\Text;
use Zend\Form\Element\Collection;

class ContatoFieldset extends BaseFieldset
{
    public function __construct($name = 'contato')
    {
        parent::__construct($name);
    }
    
    public function init()
    {
        $id        = new Hidden('id');
        $nome      = new Text('nome', array(
            'label' => 'Nome',
        ));
        
        $telefones = new Collection('telefones', array(
            'type'                   => 'Contato\Form\Telefone',
            'count'                  => 1,
            'should_create_template' => true,
            'allow_add'              => true,
        ));
        
        $this->add($id)
        $this->add($nome);
        $this->add($telefones);
    }

    public function getInputFilterSpecification()
    {
        return array(
            'id' => array(
                'required' => false,
                'filters' => array(
                    array('name' => 'Digits'),
                ),
            ),
            'nome' => array(
                'required' => true,
                'filters' => array(
                    array('name' => 'StringTrim'),
                    array('name' => 'StripTags'),
                ),
                'validators' => array(
                    'name' => 'StringLength',
                    'options' => array(
                        'min' => 3,
                        'max' => 50,
                    ),
                ),
            ),
        );
    }
}

