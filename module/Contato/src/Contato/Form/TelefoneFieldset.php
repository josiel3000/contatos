<?php

namespace Contato\Form;

use Zend\Form\Element\Hidden;
use Zend\Form\Element\Text;

class TelefoneFieldset extends BaseFieldset
{
    public function __construct($name = 'telefone')
    {
        parent::__construct($name);
    }
    
    public function init()
    {
        $id     = new Hidden('id');
        $ddd    = new Text('ddd', array(
            'label' => 'DDD',
        ));
        $numero = new Text('numero', array(
            'label' => 'Número',
        ));
        
        $this->add($id);
        $this->add($ddd);
        $this->add($numero);
    }
    
    public function getInputFilterSpecification()
    {
        return array(
            'id' => array(
                'required' => false,
                'filters' => array(
                    array('name' => 'Digits'),
                ),
            ),
            'ddd' => array(
                'required' => true,
                'filters' => array(
                    array('name' => 'Digits'),
                ),
                'validators' => array(
                    'name' => 'StringLength',
                    'options' => array(
                        'min' => 2,
                        'max' => 2,
                    ),
                ),
            ),
            'numero' => array(
                'required' => true,
                'filters' => array(
                    array('name' => 'Digits'),
                ),
                'validators' => array(
                    'name' => 'StringLength',
                    'options' => array(
                        'min' => 8,
                        'max' => 9,
                    ),
                ),
            ),
        );
    }
}
